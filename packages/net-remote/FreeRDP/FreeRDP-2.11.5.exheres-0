# Copyright 2013 Pierre Lejeune <superheron@gmail.com>
# Copyright 2014-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake
require ffmpeg [ with_opt=true ]

SUMMARY="FreeRDP: A Remote Desktop Protocol implementation"
DESCRIPTION="
FreeRDP is a free implementation of the Remote Desktop Protocol (RDP), released under the Apache license.
"
HOMEPAGE+=" https://www.freerdp.com"
DOWNLOADS="https://pub.freerdp.com/releases/${PNV,,}.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    aac
    alsa
    cups
    kerberos
    mp3
    pcsc [[ description = [ Support for smartcard authentification via pcsc-lite ] ]]
    pulseaudio
    server [[ description = [ Build the FreeRDP server ] ]]
    soxr [[ description = [ Support for audio resampling via libsoxr ] ]]
    systemd
    wayland
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( libc: musl )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        virtual/pkg-config
    build+run:
        dev-libs/icu:=
        dev-libs/libusb:1
        sys-libs/pam
        sys-libs/zlib
        x11-libs/cairo
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libXv
        x11-libs/libxkbfile
        aac? (
            media-libs/faac
            media-libs/faad2
        )
        alsa? ( sys-sound/alsa-lib )
        cups? ( net-print/cups )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        libc:musl? ( dev-libs/libexecinfo )
        mp3? ( media-sound/lame )
        pcsc? ( sys-apps/pcsc-lite )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        pulseaudio? ( media-sound/pulseaudio )
        server? ( x11-libs/libXdamage )
        soxr? ( media-libs/soxr )
        systemd? ( sys-apps/systemd )
        wayland? (
            sys-libs/wayland
            x11-libs/libxkbcommon
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/13df840d233079c1dcbe5174e820cb7aeaa2857c.patch
    "${FILES}"/${PN}-Fixde-Wimplicit-function-declaration.patch
)

CMAKE_SOURCE=${WORKBASE}/${PNV,,}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DBUILTIN_CHANNELS:BOOL=FALSE
    -DWITH_CAIRO:BOOL=TRUE
    -DWITH_CCACHE:BOOL=FALSE
    -DWITH_CHANNELS:BOOL=TRUE
    -DWITH_CLANG_FORMAT:BOOL=FALSE
    -DWITH_CLIENT:BOOL=TRUE
    -DWITH_CLIENT_CHANNELS:BOOL=TRUE
    -DWITH_CLIENT_COMMON:BOOL=TRUE
    -DWITH_DSP_EXPERIMENTAL:BOOL=FALSE
    -DWITH_GPROF:BOOL=FALSE
    -DWITH_GSM:BOOL=FALSE
    -DWITH_ICU:BOOL=TRUE
    -DWITH_INTERNAL_MD4:BOOL=TRUE
    -DWITH_INTERNAL_MD5:BOOL=TRUE
    -DWITH_IPP:BOOL=FALSE
    -DWITH_JPEG:BOOL=TRUE
    -DWITH_MANPAGES:BOOL=TRUE
    -DWITH_MBEDTLS:BOOL=FALSE
    -DWITH_MEDIACODEC:BOOL=FALSE
    -DWITH_OPENCL:BOOL=FALSE
    -DWITH_OPENH264:BOOL=FALSE
    -DWITH_OPENH264_LOADING:BOOL=FALSE
    -DWITH_OPENSLES:BOOL=FALSE
    -DWITH_OPENSSL:BOOL=TRUE
    -DWITH_OSS:BOOL=TRUE
    -DWITH_PROFILER:BOOL=FALSE
    -DWITH_SAMPLE:BOOL=FALSE
    -DWITH_SANITIZE_ADDRESS:BOOL=FALSE
    -DWITH_SANITIZE_MEMORY:BOOL=FALSE
    -DWITH_SANITIZE_THREAD:BOOL=FALSE
    -DWITH_SMARTCARD_INSPECT:BOOL=FALSE
    -DWITH_SWSCALE:BOOL=FALSE
    -DWITH_THIRD_PARTY:BOOL=FALSE
    -DWITH_VAAPI:BOOL=FALSE
    -DWITH_VALGRIND_MEMCHECK:BOOL=FALSE
    -DWITH_VERBOSE_WINPR_ASSERT:BOOL=FALSE
    -DWITH_WINPR_TOOLS:BOOL=TRUE
    -DWITH_X11:BOOL=TRUE
    -DWITH_XCURSOR:BOOL=TRUE
    -DWITH_XEXT:BOOL=TRUE
    -DWITH_XFIXES:BOOL=TRUE
    -DWITH_XI:BOOL=TRUE
    -DWITH_XINERAMA:BOOL=TRUE
    -DWITH_XKBFILE:BOOL=TRUE
    -DWITH_XRANDR:BOOL=TRUE
    -DWITH_XRENDER:BOOL=TRUE
    -DWITH_XSHM:BOOL=TRUE
    -DWITH_XV:BOOL=TRUE
    -DWITH_ZLIB:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'aac FAAC'
    'aac FAAD2'
    'alsa ALSA'
    'cups CUPS'
    'ffmpeg DSP_FFMPEG'
    'ffmpeg FFMPEG'
    'kerberos GSSAPI'
    'mp3 LAME'
    'pcsc PCSC'
    'pulseaudio PULSE'
    'server SERVER'
    'server SERVER_INTERFACE'
    'soxr SOXR'
    'systemd LIBSYSTEMD'
    'wayland WAYLAND'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_test() {
    esandbox allow_net "unix:${TEMP%/}/.pipe/*"
    default
    esandbox disallow_net "unix:${TEMP%/}/.pipe/*"
}

