# Copyright 2012 Julien Pivotto <roidelapluie@gmail.com>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_install

if ever is_scm; then
    require github [ user=mobile-shell ]
    require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
else
    DOWNLOADS="https://mosh.org/${PNV}.tar.gz"
fi

SUMMARY="Mosh is a robust replacement for SSH"
DESCRIPTION="
Remote terminal application that allows roaming, supports
intermittent connectivity, and provides intelligent local echo and line
editing of user keystrokes. Mosh is a replacement for SSH. It's more robust
and responsive, especially over Wi-Fi, cellular, and long-distance links.
"
HOMEPAGE="https://mosh.org/"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    ( providers: libressl nettle openssl )
"

DEPENDENCIES="
    build+run:
        dev-lang/perl:* [[ note = [ perl deps needed for client only ] ]]
        dev-libs/protobuf:=
        dev-perl/IO-Tty
        net-misc/openssh
        sys-libs/ncurses
        providers:libressl? ( dev-libs/libressl:= )
        providers:nettle? ( dev-libs/nettle:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=enable-fast-install
    --disable-completion
    --disable-ufw
    --without-utempter
    --enable-client
    --enable-server
    --disable-examples
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    'providers:libressl --with-crypto-library=openssl'
    'providers:nettle --with-crypto-library=nettle'
    'providers:openssl --with-crypto-library=openssl'
)

# Tests use network, skip them
RESTRICT="test"

mosh_src_install() {
    default

    [[ -d "${IMAGE}"/etc ]] && edo rmdir "${IMAGE}"/etc
}

