# Copyright 2010-2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Copyright 2012-2013 Lasse Brun <bruners@gmail.com>
# Copyright 2013-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mumble-1.2.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

MY_PN=${PN/murmur/mumble}

require github [ user=mumble-voip release=v${PV} suffix=tar.gz ] \
    cmake \
    systemd-service

SUMMARY="Mumble is an open source, low-latency, high quality voice chat software"
DESCRIPTION="
Mumble is a voice chat application for groups. While it can be used for any kind of
activity, it is primarily intended for gaming. It can be compared to programs like Ventrilo or
TeamSpeak. People tend to simplify things, so when they talk about Mumble they either talk about
\"Mumble\" the client application or about \"Mumble & Murmur\" the whole voice chat application suite.
"
HOMEPAGE+=" https://mumble.info"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    avahi
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.41.0]
        virtual/pkg-config
    build+run:
        group/${PN}
        user/${PN}
        dev-libs/protobuf:=
        sys-libs/libcap
        x11-libs/qtbase:5[?gui][sql]
        avahi? ( net-dns/avahi[dns_sd] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        !media-sound/mumble [[
            description = [ voip/murmur was previously part of media-sound/mumble ]
            resolution = uninstall-blocked-before
        ]]
"

CMAKE_SOURCE=${WORKBASE}/${MY_PN}-${PV}.src

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/f4cea62ed95e4967d8591f25e903f5e8fc2e2a30.patch
    "${FILES}"/${PN}-1.4.287-protobuf.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_NUMBER:STRING=$(ever range 3)
    -Dclient:BOOL=FALSE
    -Ddbus:BOOL=FALSE
    -Dgrpc:BOOL=FALSE
    -Dice:BOOL=FALSE
    -Dserver:BOOL=TRUE
    -Dtests:BOOL=FALSE
    -Dwarnings-as-errors:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'avahi zeroconf'
)

src_prepare() {
    cmake_src_prepare

    edo sed \
        -e 's:mumble-server:murmur:g' \
        -e 's:/var/run:/run:g' \
        -i scripts/murmur.ini

    edo sed \
        -e 's:murmur.ini:murmur/murmur.ini:' \
        -i scripts/murmur.service

    # not needed for the server part
    edo sed \
        -e '/FindPythonInterpreter/d' \
        -i cmake/qt-utils.cmake
}

src_install() {
    cmake_src_install

    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{mumble-server,murmurd}
    edo mv "${IMAGE}"/usr/share/man/man1/{mumble-server,murmurd}.1
    edo rm "${IMAGE}"/usr/share/man/man1/mumble-server-user-wrapper.1

    insinto /etc/murmur
    doins "${CMAKE_SOURCE}"/scripts/murmur.ini

    edo chown murmur:murmur "${IMAGE}"/etc/murmur/murmur.ini
    edo chmod 0640 "${IMAGE}"/etc/murmur/murmur.ini

    #insinto /usr/share/murmur/
    #doins src/murmur/Murmur.ice

    keepdir /var/{lib,log}/murmur
    edo chown murmur:murmur "${IMAGE}"/var/{lib,log}/murmur

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/murmur 0755 murmur murmur
EOF
}

