# Copyright 2014-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

JUNIT_PV=4.11

require ant

SUMMARY="Allows Tomcat to use native resources for performance, compatibility, etc"
HOMEPAGE="https://tomcat.apache.org"
DOWNLOADS="
    mirror://apache/tomcat/tomcat-connectors/native/${PV}/source/${PNV}-src.tar.gz
    https://repo1.maven.org/maven2/junit/junit/${JUNIT_PV}/junit-${JUNIT_PV}.jar
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/apr:1[>=1.7.4]
        www-servers/tomcat-bin
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=3.0.13] )
"

WORK=${WORK}-src

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-openssl-version-check
    --disable-insecure-export-ciphers
    --disable-static
    --with-apr=/usr/$(exhost --target)/bin/apr-1-config
    --with-java-home=/usr/$(exhost --target)/lib/jdk
)

ANT_SRC_PREPARE_PARAMS=( prepare )
ANT_SRC_COMPILE_PARAMS=( compile jar )
ANT_SRC_TEST_PARAMS=( test )

src_prepare() {
    local JUNIT=junit-${JUNIT_PV}
    edo mkdir -p "${WORKBASE}"/${JUNIT}
    edo cp "${FETCHEDDIR}"/${JUNIT}.jar "${WORKBASE}"/${JUNIT}/${JUNIT}.jar
    edo sed \
        -e "/base.path=/s:/usr/share/java:${WORKBASE}:" \
        -i build.properties.default

    ant_src_prepare
}

src_configure() {
    pushd native
    default
    popd
}

src_compile() {
    ant_src_compile

    pushd native
    default
    popd
}

src_install() {
    # Fetch the installation target directory from the service because the config-
    # protected configuration might not yet have been updated.
    local install_dir=$(sed -n '/ExecStart=/s:.*=.*"\(.*\)/catalina.sh.*":\1:p' /usr/$(exhost --target)/lib/systemd/system/tomcat.service)

    dodir "${install_dir}"
    insinto "${install_dir}"
    newins dist/${PNV}.jar ${PNV}.jar

    pushd native
    default
    popd
}

